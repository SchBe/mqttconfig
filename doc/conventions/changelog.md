**12. Backbone**

12.1. Schreibe für jeden Pull request einen Eintrag im **[CHANGELOG](../../CHANGELOG.md)** unter dem Punkt Unreleased.

12.2. Ordne deine Veränderungen einer der folgenden Kategorien zu: Added, Changed, Deprecated, Removed, Fixed.

12.3 Schreibe keine Romane, sondern komme auf den Punkt! Die release notes sollen kurz und prägnant über Neuerungen und Änderungen informieren.

12.4 Vermeide Fachjargon. Der Changelog soll den Anwender und Entwickler informieren.

12.5 Die Sprache für neue Einträge im Changelog ist Deutsch.